#!/usr/bin/python3

import epitran
from epitran.backoff import Backoff

backoff = Backoff(['eng-Latn'], cedict_file='cedict_1_0_ts_utf-8_mdbg.txt')
epi = epitran.Epitran('eng-Latn')

print('converting to ipa')
with open('source.txt', 'r', encoding="utf-8") as source_input:
    with open('ipa.txt', 'w+', encoding="utf-8") as ipa_output:
        count=0
        for line in source_input:
            non_numeric_line = ''.join([c.replace('*', '') for c in line if not c.isdigit()])
            print(non_numeric_line)
            epitext = epi.trans_list(non_numeric_line)
            print(epitext)
            ipa_output.write("%s\t%s\n" % (non_numeric_line, ' '.join(epitext)))
            ipa_output.flush()
            print('%d converted to IPA' % count, end="\r")
            count+=1


print('converting to sampa')
with open('source.txt', 'r', encoding="utf-8") as source_input:
    with open('sampa.txt', 'w+', encoding="utf-8") as sampa_output:
        count=0
        for line in source_input:
            non_numeric_line = ''.join([c for c in line if not c.isdigit()])
            sampa_output.write("%s\t%s\n" % (non_numeric_line, ' '.join(epi.xsampa_list(non_numeric_line))))
            sampa_output.flush()
            print('%d converted to IPA' % count, end="\r")
            count+=1

# print(backoff.transliterate('Id nec Aperol Spritz argumentum, te melius erroribus vix. '))
exit()
