package com.kaldiwavpreprocess;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.util.Dictionary;
import java.util.Map;

import com.kaldiwavpreprocess.Services.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicSliderUI;

public class MainScrean {
    private JButton openFileButton;
    private JButton exportFeaturesButton;
    private String openedFilePath;
    public JPanel panelMain;
    private JLabel exportText;
    private JLabel openText;
    private FileReaderService fileReaderService = new FileReaderServiceImpl();
    private SoundService soundService = new SoundServiceImpl();
    private IPAConverterService ipaConverterService;
    private SAMPAConverterService sampaConverterService;
    private String openedText;

    public MainScrean() {
        openFileButton.addActionListener(e -> {
            try {
                JFileChooser jFileChooser = new JFileChooser();
                //jFileChooser.setFileFilter(new FileNameExtensionFilter("mp3 & wav", "wav", "mp3"));
                jFileChooser.setFileFilter(new FileNameExtensionFilter("TEXT", "txt"));
                if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    openedFilePath = jFileChooser.getSelectedFile().getAbsolutePath();
                    openedText = fileReaderService.ReadLineByLine(openedFilePath);
                    //openText.setText("<html>" + openedText.replaceAll("\n", "<br/>") + "</html>");
                }
            } catch (Exception exception){
                System.out.println("EXCEPTION: " + exception.getMessage());
            }
        });

        exportFeaturesButton.addActionListener(e -> {
            try {
                Map<String, String> dictionary = fileReaderService.ReadIntoDictionary("ipadict.txt", ":");
                ipaConverterService = new IPAConverterServiceImpl(dictionary);
                sampaConverterService = new SAMPAConverterServiceImpl();
                FileChannel src = new FileInputStream(openedFilePath).getChannel();
                FileChannel dest = new FileOutputStream("source.txt").getChannel();
                dest.transferFrom(src, 0, src.size());
                dest.close();
                String command = "py englishConverter\\converter.py";
                Process proc = Runtime.getRuntime().exec(command);

                BufferedReader stdInput = new BufferedReader(new
                        InputStreamReader(proc.getInputStream()));

                BufferedReader stdError = new BufferedReader(new
                        InputStreamReader(proc.getErrorStream()));

                // Read the output from the command
                System.out.println("Here is the standard output of the command:\n");
                String s = null;
                while ((s = stdInput.readLine()) != null) {
                    System.out.println(s);
                }

                // Read any errors from the attempted command
                System.out.println("Here is the standard error of the command (if any):\n");
                while ((s = stdError.readLine()) != null) {
                    System.out.println(s);
                }

                String ipaText = fileReaderService.ReadLineByLine("ipa.txt");
                String sampaText = fileReaderService.ReadLineByLine("sampa.txt");
                //exportText.setText("<html>" + "IPA: <br/>" + ipaText.replaceAll("\n", "<br/>") +"<br/>" +"Sampa: <br/>" + sampaText.replaceAll("\n", "<br/>") + "</html>");
                //JOptionPane.showMessageDialog(null, "exportFile");
            /*String[] kaldiInput = {"-a", openedFilePath};
            KaldiMain.KaldiMain(kaldiInput);*/ //KALDI USAGE
            } catch (Exception exception){
                System.out.println("EXCEPTION: " + exception.getMessage());
            }
        });
    }
}
