package com.kaldiwavpreprocess.Services;

import java.util.HashMap;
import java.util.Map;

public class SAMPAConverterServiceImpl implements SAMPAConverterService{
    private final Map<String,String> IPA_PHONE_TO_SAMPA = new HashMap<String,String>();
    {
        IPA_PHONE_TO_SAMPA.put("ɑ", "A");
        IPA_PHONE_TO_SAMPA.put("æ", "{");
        IPA_PHONE_TO_SAMPA.put("ɐ", "6");
        IPA_PHONE_TO_SAMPA.put("ɒ", "Q");
        IPA_PHONE_TO_SAMPA.put("ɛ", "E");
        IPA_PHONE_TO_SAMPA.put("ə", "@");
        IPA_PHONE_TO_SAMPA.put("ɜ", "3");
        IPA_PHONE_TO_SAMPA.put("ɪ", "I");
        IPA_PHONE_TO_SAMPA.put("ɔ", "O");
        IPA_PHONE_TO_SAMPA.put("ø", "2");
        IPA_PHONE_TO_SAMPA.put("œ", "9");
        IPA_PHONE_TO_SAMPA.put("ɶ", "&");
        IPA_PHONE_TO_SAMPA.put("ʊ", "U");
        IPA_PHONE_TO_SAMPA.put("ʉ", "}");
        IPA_PHONE_TO_SAMPA.put("ʌ", "V");
        IPA_PHONE_TO_SAMPA.put("ʏ", "Y");
        IPA_PHONE_TO_SAMPA.put("β", "B");
        IPA_PHONE_TO_SAMPA.put("ç", "C");
        IPA_PHONE_TO_SAMPA.put("ð", "D");
        IPA_PHONE_TO_SAMPA.put("ɣ", "G");
        IPA_PHONE_TO_SAMPA.put("ʎ", "L");
        IPA_PHONE_TO_SAMPA.put("ɲ", "J");
        IPA_PHONE_TO_SAMPA.put("ŋ", "N");
        IPA_PHONE_TO_SAMPA.put("ʁ", "R");
        IPA_PHONE_TO_SAMPA.put("ʃ", "S");
        IPA_PHONE_TO_SAMPA.put("θ", "T");
        IPA_PHONE_TO_SAMPA.put("ɥ", "H");
        IPA_PHONE_TO_SAMPA.put("ʒ", "Z");
        IPA_PHONE_TO_SAMPA.put("ʔ", "?");
        IPA_PHONE_TO_SAMPA.put("ː", ":");
        IPA_PHONE_TO_SAMPA.put("ˈ", "\"");
        IPA_PHONE_TO_SAMPA.put("ˌ", "%");
        IPA_PHONE_TO_SAMPA.put(" ", "=n");
        IPA_PHONE_TO_SAMPA.put("~", "O~");
        IPA_PHONE_TO_SAMPA.put("\n", "\n");
        IPA_PHONE_TO_SAMPA.put(" ", " ");
    }

    public String IPAPhoneToSAMPA(String ipaText) {
        StringBuilder sampa = new StringBuilder();

        for (int i=0; i<ipaText.length(); i++) {
            String ipaPhoneme = ipaText.substring(i, i+1);
            String sampaPhoneme = IPA_PHONE_TO_SAMPA.get(ipaPhoneme) == null ? " NOT_RECOGNIZED " : IPA_PHONE_TO_SAMPA.get(ipaPhoneme);
            if (sampaPhoneme == null) {
                //throw new IllegalArgumentException("Unrecognized LIA_phon phoneme: " + ipaPhoneme);
            }
            sampa.append(sampaPhoneme);
        }
        return sampa.toString();
    }
}
