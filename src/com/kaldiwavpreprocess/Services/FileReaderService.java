package com.kaldiwavpreprocess.Services;

import java.util.Map;

public interface FileReaderService {
    String ReadLineByLine(String filePath);
    Map<String, String> ReadIntoDictionary(String filePath, String separator);
}
