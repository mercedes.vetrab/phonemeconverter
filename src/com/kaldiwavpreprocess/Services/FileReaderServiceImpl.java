package com.kaldiwavpreprocess.Services;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Map;
import java.util.stream.Stream;

public class FileReaderServiceImpl implements FileReaderService {

    public String ReadLineByLine(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public Map<String, String> ReadIntoDictionary(String filePath, String separator) {
        Map<String, String> resultDictionary = new Hashtable<String, String>();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> {
                if(s.split(separator) != null && s.split(separator).length == 2){
                    resultDictionary.put(
                            s.split(separator)[0]
                                    .replaceAll("^\\s", "")
                                    .replaceAll("\\s$", ""),
                            s.split(separator)[1]
                                    .replaceAll("^\\s", "")
                                    .replaceAll("\\s$", ""));
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultDictionary;
    }

}
