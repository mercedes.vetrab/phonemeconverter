package com.kaldiwavpreprocess.Services;

public interface SoundService {
    void playSound(String filePath);
}
