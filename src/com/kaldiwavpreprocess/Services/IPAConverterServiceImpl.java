package com.kaldiwavpreprocess.Services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class IPAConverterServiceImpl implements IPAConverterService {
    private Map<String, String> phoneticDictionary;

    public IPAConverterServiceImpl(Map<String, String> dictionary){
        phoneticDictionary = new HashMap<>();
        dictionary.forEach((key, value) -> phoneticDictionary.put(key.toLowerCase(), value));
    }

    @Override
    public String ConvertTextToIPA(String text) {
        return Arrays.stream(text.split("\n")).map(paragraph -> {
            return Arrays.stream(paragraph.split(" ")).map(word -> {
                String searchedWord = word.replaceAll("([\\.,!?])", "");
                return (phoneticDictionary.get(searchedWord.toLowerCase()) == null ? "NOT_FOUND " : phoneticDictionary.get(searchedWord.toLowerCase()));
            }).collect(Collectors.joining(" "));
        }).collect(Collectors.joining("\n"));
    }

}
