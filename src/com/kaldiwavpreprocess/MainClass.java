package com.kaldiwavpreprocess;

import javax.swing.*;

public class MainClass {

    public static void main(String[] args){
        JFrame mainFrame = new JFrame("MainScreen");
        mainFrame.setContentPane(new MainScrean().panelMain);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setSize(800,800);
        mainFrame.setVisible(true);
    }
}
